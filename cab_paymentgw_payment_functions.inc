<?php
/**
 * @file
 * Commerce Account Balance Payment Gateway payment methods include file.
 *
 * Author:
 * Tamas Kolossvary
 * tamas@kolossvary.hu.
 */

/**
 * Payment method submit form.
 */
function cab_paymentgw_submit_form($payment_method, $pane_values, $checkout_pane, $order) {

  $text = '';

  $amount = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'];
  $currency_code = $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code'];

  // Get balance.
  $balance = cab_paymentgw_calculate_balance($order->uid);

  if ($balance['balance'] >= $amount) {
    $balance_after = $balance['balance'] - $amount;

    $text .= theme('table',
      array(
        'header' => array(),
        'rows' => array(
          array(
            t('Amount:'),
            commerce_currency_format($amount, $currency_code),
          ),
          array(
            t('Account Balance:'),
            $balance['balance_display'],
          ),
          array(
            t('Balance after:'),
            commerce_currency_format($balance_after, $currency_code),
          ),
        ),
      )
    );
  }
  else {
    $text .= t("You don't have enough balance for this purchase.");
  }

  $form['text'] = array(
    '#markup' => $text,
  );

  return $form;
}

/**
 * Payment method submit form validate.
 *
 * Check for enough balance for the payment.
 */
function cab_paymentgw_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {

  $amount = $order->commerce_order_total[LANGUAGE_NONE][0]['amount'];
  $balance = cab_paymentgw_calculate_balance($order->uid);

  if ($balance['balance'] >= $amount) {
    return TRUE;
  }
  else {
    drupal_set_message(t("You don't have enough balance for this purchase."), 'error');
    return FALSE;
  }
}

/**
 * Payment method submit form submit function.
 */
function cab_paymentgw_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {

  $amount = -1 * $order->commerce_order_total[LANGUAGE_NONE][0]['amount'];

  // Create purchase transaction.
  $values = array(
    'action' => t('Purchase by account balance'),
    'order_id' => $order->id,
    'uid' => $order->uid,
    'type' => 'purchase',
    'amount' => $amount,
    'time' => time(),
  );
  entity_create('commerce_account_balance_transaction', $values)->save();

}

/**
 * Calculate user balance raw value and display.
 */
function cab_paymentgw_calculate_balance($uid) {
  $result = array();

  $user_wrapper = entity_metadata_wrapper('user', $uid);
  $result['balance_display'] = $user_wrapper->account_balance->raw();
  $result['balance'] = preg_replace("/[^-0-9]/", "", $result['balance_display']);

  return $result;
}
