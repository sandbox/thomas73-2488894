This is a Drupal Commerce Payment Gateway for the Commerce Account Balance
module. You can use your account balance for paying in the normal Drupal
Commerce checkout process, not only the simple purchase mode provided by
Commerce Account Balance module.
